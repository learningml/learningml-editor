FROM node:16.14.2
WORKDIR /app
COPY . /app
RUN npm install
CMD ["/app/node_modules/@angular/cli/bin/ng", "serve", "--host", "0.0.0.0"]
