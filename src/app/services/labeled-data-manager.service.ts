import { Injectable } from '@angular/core';
import { ExampleData, Label, MLState, IProject, ILabeledData } from '../interfaces/interfaces';
import { ProjectManagerService } from './project-manager.service';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { saveAs } from 'file-saver-es';

@Injectable({
  providedIn: 'root'
})
export class LabeledDataManagerService {

  labelsWithData: Map<Label, ExampleData[]>;
  modelType = null;
  mlAlgorithm = null;
  labels: Label[];
  id: number = null;
  name = 'sin nombre';
  state = MLState.EMPTY;
  advanced = false;
  featureDimension = 2;
  featureDimensionLocked = false;
  algorithms_params = {
    validationSplit: 0,
    knn: {
      k: 5
    },
    neural_network: {
      epochs: 20,
      batchSize: 10,
      learningRate: 0.001
    }
  };

  constructor(private projectManager: ProjectManagerService) {
    this.labelsWithData = new Map<Label, ExampleData[]>();
    this.labels = [];
  }

  getTotalNumberOfExamples(){
    let n = 0;
    for (let label of this.labels){
      n += this.labelsWithData.get(label).length
    }
    return n;
  }

  clear() {
    this.labels = [];
    this.labelsWithData.clear();
  }

  get(label: Label) {
    return this.labelsWithData.get(label);
  }

  save() {
    const blob = new Blob([this.serializeModel()], { type: 'application/json' });
    saveAs(blob, this.name);
  }

  addLabel(label: string) {
    // Add label only if not exits
    if (label == "") return;
    if (!this.labelsWithData.has(label)) {
      //this.labels.push(label);
      // esto es para detectar cambios en el array
      this.labels = [label].concat(this.labels);
      this.labelsWithData.set(label,  []);
    }

  }

  removeLabel(label: string) {
    this.state = MLState.UNTRAINED;

    // Esto es una manera INMUTABLE de borrar un elemento de un array
    // Necesito que sea una operación inmutable para que angular se 
    // dé cuenta de que el array ha cambiado.
    this.labels = this.labels.filter(o => o != label);

    this.labelsWithData.delete(label);
    if(this.labels.length == 0) this.state = MLState.EMPTY;
  }

  addEntry(entry: ILabeledData) {
    this.state = MLState.UNTRAINED;
    let label = entry.label;
    let data = entry.data;
    if (label == "" || data == "" ||
      label == null || data == null ||
      label == undefined || data == undefined) return;
    this.addLabel(label);
    if (this.labelsWithData.get(label).indexOf(data) == -1) {
      this.labelsWithData.set(label, this.labelsWithData.get(label).concat(data));
    }
  }

  removeEntry(entry: ILabeledData) {
    this.state = MLState.UNTRAINED;
    let label = entry.label;
    let data = entry.data;

    this.labelsWithData.set(label, this.labelsWithData.get(label).filter(o => o != data))
  }

  setTraindata() {

  }

  truncateNumbers(numbersCSV){
    let numbers = [];
    let numbersString = numbersCSV.split(",");
    for(let numberString of numbersString){
      numbers.push(parseFloat(numberString).toFixed(4));
    }

    return numbers.join(",");
  }

  load(file: string) {
    let inputData;

    this.clear();

    try {
      inputData = JSON.parse(file);
      Object.keys(inputData.data).forEach(key => {
        this.modelType = inputData.type;
        let data = inputData.data[key];
        for (let d of data) {
          if (this.modelType == 'text') {
            this.addEntry({ label: key, data: d });
          }
          if (this.modelType == 'numerical'){
            let _d = this.truncateNumbers(d);
            this.addEntry({label: key, data: _d});
            this.featureDimension = d.split(",").length;
            this.featureDimensionLocked = true;
          }
          if (this.modelType == 'image') {
            let i = new Image();
            i.src = d;
            this.addEntry({ label: key, data: i });
          }
        }
      });
    }
    catch{
      alert("Fichero erróneo. No puedo interpretar ese fichero. ¿Seguro que está bien construido?");
    }
  }

  saveOnServer(asCopy: boolean): Observable<any> {

    const body: IProject = {
      name: this.name,
      type: this.modelType,
      json_data: this.serializeModel()
    }

    let requestObservable: Observable<IProject>;

    if (this.id && !asCopy) {
      body["id"] = this.id;
      requestObservable = this.projectManager.updateProject(this.id, body);
    } else {
      if(asCopy){
        body.name = body.name + '-copy';
      }
      requestObservable = this.projectManager.createProject(body)
        .pipe(map(v => {
          this.id = v.id;
          return v;
        }));
    }

    return requestObservable
  }

  loadFromServer(id) {

    let requestObservable: Observable<IProject>;

    requestObservable = this.projectManager.getProject(id)
      .pipe(map((v: IProject) => {
        let inputData = JSON.parse(v.json_data);
        this.name = v.name;
        this.id = v.id;
        if (this.modelType == "text") {
          Object.keys(inputData.data).forEach(key => {
            let data = inputData.data[key];
            for (let d of data) {
              if (this.modelType == "text") {
                this.addEntry({ label: key, data: d });
              }
            }
          });
        }
        if (this.modelType == 'image') {
          this.load(v.json_data)
        }

        return v;
      }));

    return requestObservable;
  }

  serializeModel(): string {
    let dataObject = {
      type: this.modelType,
      data: {}
    };
    for (let label of this.labelsWithData.keys()) {
      dataObject.data[label] = [];
      for (let data of this.labelsWithData.get(label)) {
        if (this.modelType == "text" || this.modelType == "numerical") {
          dataObject.data[label].push(data);
        }
        if (this.modelType == "image") {
          dataObject.data[label].push(data['src']);
        }

      }
    }
    let dataJSON = JSON.stringify(dataObject);

    return dataJSON;
  }
}
