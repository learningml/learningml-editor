import { TestBed } from '@angular/core/testing';

import { FeatureExtractionService } from './feature-extraction.service';

describe('FeatureExtractionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FeatureExtractionService = TestBed.get(FeatureExtractionService);
    expect(service).toBeTruthy();
  });
});
