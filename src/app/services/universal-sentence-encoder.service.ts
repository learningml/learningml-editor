import { Injectable } from '@angular/core';
import * as universalSentenceEncoder from '@tensorflow-models/universal-sentence-encoder';


@Injectable({
  providedIn: 'root'
})
export class UniversalSentenceEncoderService {

  use = null;

  constructor() { }

  async get() {
    if (!this.use) {
      this.use = await universalSentenceEncoder.load();
    }

    return this.use;
  }
}
