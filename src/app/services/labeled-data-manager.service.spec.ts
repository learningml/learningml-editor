import { TestBed } from '@angular/core/testing';

import { LabeledDataManagerService } from './labeled-data-manager.service';

describe('LabeledDataManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LabeledDataManagerService = TestBed.get(LabeledDataManagerService);
    expect(service).toBeTruthy();
  });
});
