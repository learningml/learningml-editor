import * as BrainText from 'brain-text';
import * as tf from '@tensorflow/tfjs';
import { ExampleData, TText, Label } from '../../interfaces/interfaces';

export class FeatureExtractorText {

  private brainText: BrainText; 
  private classes;
  private labelsWithData;

  constructor() { }

  static async Create(labelsWithData: Map<Label, ExampleData[]>, lang: string) {
    const me = new FeatureExtractorText();
    //right now lang is not necessary, we keep it for a possible future use
    me.labelsWithData = labelsWithData;
    me.brainText = new BrainText(lang);

    return me;
  }

  /**
 * Build an object wich maps classes names to number and the dictionary
 * which will be used to vectorize words.
 * @param {*} traindata 
 * @param {*} bow 
 */
  buildClasses(traindata) {
    this.classes = {};
    // extract all the classe (which are the labels) without
    // repetition from traindata and map to number in order to
    // feed the ANN
    let i = 0;
    for (let data of traindata) {
      if (this.classes[data.label] == undefined) {
        this.classes[data.label] = i;
        i++;
      }
    }
  }

  getRawDataset() {
    let rawDataset = [];
    for (let label of this.labelsWithData.keys()) {
      for (let data of this.labelsWithData.get(label)) {
        let e = { label: label, text: data }
        rawDataset.push(e)
      }
    }
    return rawDataset;
  }

  extractInstanceFeatures(text: TText): Promise<tf.Tensor> {
    let feature = this.brainText.extractFeature(text);
    let tensor_inputs = tf.cast(tf.stack(feature), 'int32');

    return new Promise((resolve, reject) => {
      let shape = tensor_inputs.shape;  
      resolve(tensor_inputs.reshape([1, shape[0]]));
    })
  }

  labelOneShotEncoding(res, num_classes) {
    let vec = [];
    for (let i = 0; i < num_classes; i += 1) {
      vec.push(0);
    }
    vec[res] = 1;
    return vec;
  }

  extractDatasetFeatures(): Promise<{ text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor }> {
    // first the map structure of data stored in labeledDataManager 
    // is reorganized as a list of pairs {label, data}
    
    let rawDataset = this.getRawDataset();
    this.buildClasses(rawDataset)
    this.brainText.setTraindata(rawDataset);
    this.brainText.updateInfrastructure();

    let traindata_for_ann = this.brainText.prepareTrainData();

    let inputs = [];
    let labels = [];


    for (let data of rawDataset) {
      inputs.push(data.text);
      labels.push(this.labelOneShotEncoding(
        this.classes[data.label], Object.keys(this.classes).length))
    }

    let text_labels  = Object.keys(this.classes)
    let tensor_labels  = tf.cast(tf.stack(labels), 'int32');
    let tensor_inputs = tf.cast(tf.stack(traindata_for_ann), 'int32');

    return new Promise((resolve, reject) => {
      resolve({ text_labels, tensor_labels, tensor_inputs });
    })
  }
}
