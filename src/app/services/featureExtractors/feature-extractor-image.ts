import * as tf from '@tensorflow/tfjs';
import { ExampleData, Label } from '../../interfaces/interfaces';

const IMAGE_SIZE = 227;

export class FeatureExtractorImage {
  ready = false;
  mobilenet = null;
  labels = [];
  dataset = {
    data: [],
    labels: [],
    dataArray: []
  };
  labelsWithData;

  constructor() { }

  static async Create(labelsWithData: Map<Label, ExampleData[]>, mobilenetService) {
    const me = new FeatureExtractorImage();
    me.labelsWithData = labelsWithData;

    /** 
     * Se carga el modelo de clasificación de imágenes Mobilenet
     * https://arxiv.org/abs/1704.04861
     */
    me.mobilenet = await mobilenetService.get();
    me.ready = true;

    return me;
  }

  extractInstanceFeatures(image: HTMLImageElement): Promise<tf.Tensor> {
    image.width = IMAGE_SIZE;
    image.height = IMAGE_SIZE;
    const t_image = tf.browser.fromPixels(image);
    const t_activation = this.mobilenet.infer(t_image, 'conv_preds');
    t_image.dispose();
    return new Promise((resolve, reject) => {
      resolve(t_activation);
    });
  }


  addLabeledImage(labeledImage) {

    // si la label (string) no existe en el array de labels, la añadimos
    if (this.labels.indexOf(labeledImage.label) == -1) {
      this.labels.push(labeledImage.label);
    }

    return this.extractInstanceFeatures(labeledImage.data).then((t_activation) => {
      let t = t_activation.squeeze();
      this.dataset.data.push(t);
      this.dataset.dataArray.push(t.dataSync());
      // Añadimos al array de labels del dataset el índice que el corresponde
      // a la label en el array labels.
      this.dataset.labels.push(this.labels.indexOf(labeledImage.label));

      t_activation.dispose();
    })

  }

  async buildDataset() {
    this.labels = [];
    this.dataset.labels = [];
    for (let t of this.dataset.data) {
      t.dispose();
    }
    this.dataset.data = [];
    this.dataset.dataArray = [];
    for (const k of this.labelsWithData.keys()) {
      for (const image of this.labelsWithData.get(k)) {
        await this.addLabeledImage({ label: k, data: image })
      }
    }
  }

  /**
   * This is the method which must be called to convert the raw data stored in
   * LabeledDataManagerService to an object containing:
   * 
   *  - the `text_labels`
   *        is an array of strings wich elements are the text labels
   *  - the `tensor_labels`
   *        is a tf.Tensor object with shape  [num_examples, num_labels], where:
   *            - num_examples, is the number of examples in the dataset and
   *            - num_labels, is the number of classes(labels) to be recognized.
   *       The elements of this tensor are Int32 data ranging from 0 to 
   *        num_labels - 1, and represent the index of the corresponding text
   *        label in `text_labels`
   *  - the `tensor_inputs`
   *        is a tf.Tensor object with shape [num_example, 1024] where:
   *            - num_examples, is the number of examples in the dataset.
   *        The images are converted in a 1024 vector when passed through the
   *        `extractFeature()` method. This explain the second element of the 
   *        shape.
   *        The elements of this tensor are Arrays of 1024 float32 numbers, and 
   *        each Array represents an image of the dataset. 
   * 
   *  
   * @returns {text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor}
   */
  extractDatasetFeatures(): Promise<{ text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor }> {
    return this.buildDataset().then(() => {
      const inputs = tf.stack(this.dataset.data);
      const labels = tf.tidy(() => tf.oneHot(tf.tensor1d(this.dataset.labels, 'int32'), this.labels.length));

      return  { text_labels: this.labels, tensor_labels: labels, tensor_inputs: inputs };
      
    });

  }
}
