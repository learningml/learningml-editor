import * as tf from '@tensorflow/tfjs';
import { MLState } from '../../interfaces/interfaces';
import { LabeledDataManagerService } from '../labeled-data-manager.service';


export class MlAlgorithmNeuralNetwork {

  mlModel = null;
  features = null;
  labeledDataManager = null;
  params = null;

  constructor(labeledDataManager: LabeledDataManagerService, params: any) {
    this.labeledDataManager = labeledDataManager;
    this.params = params;
  }

  buildModel(inputShape, outputShape) {
    /**
    * Creamos un modelo consistente en una red neuronal con una capa de entrada,
    * una capa oculta y una capa de salida. 
    * 
    * La capa de entrada recibe un tensor de forma (shape) [inputShape], es decir que hay 
    * inputShape entradas, estas se corresponden con la salida del modelo mobilenet truncada
    * hasta la capa 'conv_preds'. De manera que usamos ese modelo truncado para transformar
    * la imagen en un tensor de shape [1, 1024]. Y es ese tensor el que usamos como entrada
    * de nuestro modelo, es decir, del modelo que se especifica aquí. 
    */
    if (this.mlModel != null) {
      this.mlModel.dispose();
      this.mlModel = null;
    }

    this.mlModel = tf.sequential({
      layers: [
        tf.layers.dense({
          units: 200,
          inputShape: [inputShape],
          activation: 'relu'
        }),
        tf.layers.dense({
          units: 100,
          activation: 'relu',
          kernelInitializer: 'varianceScaling',
          useBias: true
        }),
        tf.layers.dense({
          units: outputShape,
          kernelInitializer: 'varianceScaling',
          useBias: false,
          activation: 'softmax'
        })
      ]
    });

    // 0.0001 es el learning rate
    //const optimizer = tf.train.adamax();
    const optimizer = tf.train.adam(this.params.learningRate)

    this.mlModel.compile({
      optimizer: optimizer,
      loss: 'categoricalCrossentropy',
      metrics: ['accuracy']
    });
  }

  train(features: { text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor }, validationData): Promise<tf.History> {

    this.labeledDataManager.state = MLState.TRAINING;
    const inputShape = features.tensor_inputs.shape[1];
    const labelsShape = features.tensor_labels.shape[1];
    this.buildModel(inputShape, labelsShape);
    let that = this;
    function onBatchEnd(batch, logs) {
      console.log('Accuracy', logs.acc);
    }

    //Train for 5 epochs with batch size of 32.
    return this.mlModel.fit(features.tensor_inputs, features.tensor_labels, {
      epochs: this.params.neural_network.epochs,
      batchSize: this.params.neural_network.batchSize,
      callbacks: { onBatchEnd },
      shuffle: true,
      //validationSplit: this.params.neural_network.validationSplit/100,
      validationData: validationData
    }).then(info => {
      return { mlModel: this.mlModel, info: info}
    });
  }

  classify(input_tensor: tf.Tensor, labels: string[]): Promise<[string, number][]> {
    const prediction = this.mlModel.predict(input_tensor);
    input_tensor.dispose();
    const predictions = prediction.dataSync();
    const arr_predictions = Array.from(predictions);
    let results = [];
    for (let i = 0; i < arr_predictions.length; i++) {
      results.push([labels[i], arr_predictions[i]]);
    }
    results.sort((a, b) => b[1] - a[1]);
    return new Promise((resolve, reject) => { resolve(results) });

  }
}
