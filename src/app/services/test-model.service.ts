import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LabeledDataManagerService } from 'src/app/services/labeled-data-manager.service';
import { MLState } from '../interfaces/interfaces';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class TestModelService {

  constructor(private translate: TranslateService,
    private labeledDataManager: LabeledDataManagerService,
    private snackBar: MatSnackBar) { }

  confidenceLevel(confidence, label) {
    let textLabel = "";
    if (confidence < 0.50) {
      textLabel = 'confidence_text.very_low';
    } else if (confidence >= 0.50 && confidence < 0.7) {
      textLabel = 'confidence_text.low';
    } else if (confidence >= 0.7 && confidence < 0.8) {
      textLabel = 'confidence_text.normal';
    } else if (confidence >= 0.8 && confidence < 0.9) {
      textLabel = 'confidence_text.high';
    } else if (confidence >= 0.9) {
      textLabel = 'confidence_text.very_high';
    }

    return this.translate.get(textLabel, { label: label });
  }

  mostLikelyClassification(prediction){
    let sortedPrediction = prediction.sort(function(a, b) { return b[1] - a[1]; });
    return sortedPrediction[0][0];
  }

  mostLikelyConfidence(prediction){
    let sortedPrediction = prediction.sort(function(a, b) { return b[1] - a[1]; });
    
    return sortedPrediction[0][1];
  }

  checkIfTestCanBeDone() {
    if (this.labeledDataManager.state == MLState.EMPTY || this.labeledDataManager.state == MLState.UNTRAINED) {
      this.snackBar.open("Antes hay que recopilar datos y aprender a partir de ellos",
        'Crea un modelo', {
        duration: 3000,
        verticalPosition: 'top'
      });
      return false;
    }

    return true;
  }
}
