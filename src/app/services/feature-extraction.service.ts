import { Injectable } from '@angular/core';
import * as tf from '@tensorflow/tfjs';
import { FeatureExtractorText } from './featureExtractors/feature-extractor-text';
import { FeatureExtractorImage } from './featureExtractors/feature-extractor-image';
import { FeatureExtractorNumerical } from './featureExtractors/feature-extractor-numerical';
import { LabeledDataManagerService } from './labeled-data-manager.service';
import { TText } from '../interfaces/interfaces';
import { MobilenetService } from './mobilenet.service';


@Injectable({
  providedIn: 'root'
})
export class FeatureExtractionService {

  featureExtractor = null;

  constructor(private labeledDataManager: LabeledDataManagerService,
    private mobilenetService: MobilenetService) { }

  extractInstanceFeatures(data: TText | HTMLImageElement): Promise<tf.Tensor> {
    return this.featureExtractor.extractInstanceFeatures(data);
  }

  async extractDatasetFeatures(lang: string = null): Promise<{ text_labels: string[], tensor_labels: tf.Tensor, tensor_inputs: tf.Tensor }> {

    let labelsWithData = this.labeledDataManager.labelsWithData;
    switch (this.labeledDataManager.modelType) {
      case 'text':
        this.featureExtractor = await FeatureExtractorText.Create(labelsWithData, lang);
        break;
      case 'image':
        this.featureExtractor = await FeatureExtractorImage.Create(labelsWithData, this.mobilenetService);
        break;
      case 'numerical':
        this.featureExtractor = await FeatureExtractorNumerical.Create(labelsWithData);
        break;
      default:
        break;
    }

    let extractedFeatures = this.featureExtractor.extractDatasetFeatures();
    return extractedFeatures
  }
}
