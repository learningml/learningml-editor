import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProgressSpinnerDialogComponent } from '../components/progress-spinner-dialog/progress-spinner-dialog.component';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ShowProgressSpinnerService {

  response: string;

  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  showProgressSpinnerUntilExecuted(observable: Observable<Object>,
    text: string, image: string,
    afterMessageTitle: string, afterMessage) {

    let dialogRef: MatDialogRef<ProgressSpinnerDialogComponent>
      = this.dialog.open(ProgressSpinnerDialogComponent, {
        panelClass: 'transparent',
        disableClose: true,
        data: {
          text: text,
          image: image
        }
      });

    let subscription = observable.subscribe(
      (response: any) => {
        subscription.unsubscribe();

      

        this.snackBar.open(afterMessageTitle,
          afterMessage, {
            duration: 2000,
          });
        dialogRef.close();
      },
      (error) => {
        subscription.unsubscribe();
        //handle error
        dialogRef.close();
      }
    );

    return dialogRef;
  }
}
