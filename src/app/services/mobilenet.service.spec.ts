import { TestBed } from '@angular/core/testing';

import { MobilenetService } from './mobilenet.service';

describe('MobilenetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MobilenetService = TestBed.get(MobilenetService);
    expect(service).toBeTruthy();
  });
});
