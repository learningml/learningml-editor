import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MlFooterComponent } from './ml-footer.component';

describe('MlFooterComponent', () => {
  let component: MlFooterComponent;
  let fixture: ComponentFixture<MlFooterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MlFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MlFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
