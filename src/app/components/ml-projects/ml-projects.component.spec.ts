import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MlProjectsComponent } from './ml-projects.component';

describe('MlProjectsComponent', () => {
  let component: MlProjectsComponent;
  let fixture: ComponentFixture<MlProjectsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MlProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MlProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
