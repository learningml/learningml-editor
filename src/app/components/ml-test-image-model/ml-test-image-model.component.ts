import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LabeledDataManagerService } from 'src/app/services/labeled-data-manager.service';
import { MlWebCamComponent } from '../ml-web-cam/ml-web-cam.component';
import { ScratchManagerService } from 'src/app/services/scratch-manager.service';
import { MlAlgorithmService } from 'src/app/services/ml-algorithm.service';
import { FeatureExtractionService } from 'src/app/services/feature-extraction.service';
import { TestModelService } from 'src/app/services/test-model.service';


@Component({
  selector: 'app-ml-test-image-model',
  templateUrl: './ml-test-image-model.component.html',
  styleUrls: ['./ml-test-image-model.component.css']
})
export class MlTestImageModelComponent implements OnInit {

  testImageURL: string = null;
  prediction = null;
  mostLikelyClass: string;
  confidenceText: string;
  @ViewChild('fileImagesElement', { static: true }) fileElement: ElementRef;
  @ViewChild('webcam') webcam: MlWebCamComponent;

  constructor(public labeledDataManager: LabeledDataManagerService,
    private featureExtractionService: FeatureExtractionService,
    private mlAlgorithmService: MlAlgorithmService,
    private scratchManager: ScratchManagerService,
    private testModelService: TestModelService) { }

  ngOnInit() {
  }

  takeSnapshot(b64Img) {
    let image = document.createElement('img');

    this.testImageURL = b64Img;
    image.src = b64Img;

    image.onload = () => {
      this.featureExtractionService.extractInstanceFeatures(image)
        .then(inputTensor => {
          return this.mlAlgorithmService.classify(inputTensor);
        })
        .then(prediction => {
          this.prediction = prediction;
          this.testModelService.confidenceLevel(prediction[0][1], prediction[0][0]);
        });
    }
  }

  onLoaded(e) {
    let files = e.target.files;

    for (let file of files) {
      let fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onloadend = () => {
        this.testImageURL = fileReader.result.toString();
        let image = document.createElement('img');

        image.src = fileReader.result.toString();

        image.onload = () => {
          this.featureExtractionService.extractInstanceFeatures(image)
            .then(inputTensor => {
              return this.mlAlgorithmService.classify(inputTensor); 
            })
            .then(prediction => {
              this.prediction = prediction;
              this.testModelService.confidenceLevel(prediction[0][1], prediction[0][0]);
            });
        }
      }
    }
  }


  testImage() {
    if (!this.testModelService.checkIfTestCanBeDone()) return;
    this.fileElement.nativeElement.click();
  }

  testImageFromWebcam() {
    if (!this.testModelService.checkIfTestCanBeDone()) return;
    this.webcam.setCardClass('cam-card-test');
    this.webcam.enableVideo();
  }

  loadScratch() {
    this.scratchManager.load();
  }

}
