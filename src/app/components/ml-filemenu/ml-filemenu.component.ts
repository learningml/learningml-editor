import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { LabeledDataManagerService } from 'src/app/services/labeled-data-manager.service';
import { from } from 'rxjs';
import { ShowProgressSpinnerService } from '../../services/show-progress-spinner.service';
import { ConfigService } from 'src/app/services/config.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-ml-filemenu',
  templateUrl: './ml-filemenu.component.html',
  styleUrls: ['./ml-filemenu.component.css']
})
export class MlFilemenuComponent implements OnInit {

  @ViewChild('fileElement', { static: true }) fileElement: ElementRef;

  constructor(public authService: AuthenticationService,
    public labeledDataManager: LabeledDataManagerService,
    private progressSpinner: ShowProgressSpinnerService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService) {

  }

  ngOnInit() { }

  new() {
    window.location.href = ConfigService.settings.easyml.url;
  }

  load() {
    this.fileElement.nativeElement.click();
  }

  save() {
    this.labeledDataManager.save();
  }

  saveOnServer(asCopy = false) {

    let keys_to_be_translated = [
      'filemenu.init_session.title',
      'filemenu.init_session.message',
      'filemenu.save_project.title',
      'filemenu.save_project.message',
      'filemenu.save_problem',
      'filemenu.cant_save_other_project',
      'filemenu.activate_account'
    ];

    this.translate.get(keys_to_be_translated).subscribe((res: string) => {
      if (!this.authService.isAuthenticated) {
        this.snackBar.open(res['filemenu.init_session.message'],
          res['filemenu.init_session.title'], {
          duration: 3000,
          verticalPosition: 'top'
        });
      }

      this.labeledDataManager.saveOnServer(asCopy)
        .subscribe(
          project => {
            this.snackBar.open(res['filemenu.save_project.message'],
              res['filemenu.save_project.title'], {
              duration: 3000,
              verticalPosition: 'top'
            });
            if (asCopy) {
              window.location.href = ConfigService.settings.easyml.url +
                '/model/' + project.type + '?id=' + project.id;
            }
          },
          e => {
            let message = res['filemenu.save_problem'];
            if (e.status == 403) {
              message = res['filemenu.cant_save_other_project'];
            }
            if (e.status == 401) {
              message = res['filemenu.activate_account']
            }
            this.snackBar.open(message,
              '¡Ooohhhh!', {
              duration: 3000,
              verticalPosition: 'top'
            });
          }
        );
    })
  }

  onLoaded(e) {

    let keys_to_be_translated = [
      'filemenu.loaded',
      'filemenu.loading',
    ];


    let ch_load_json = new BroadcastChannel('load-channel');
    ch_load_json.postMessage("json_loaded");

    this.translate.get(keys_to_be_translated).subscribe((res: string) => {
      let file = e.target.files[0];
      let inputDataName = file.name.replace(/\.[^/.]+$/, "");

      this.labeledDataManager.name = inputDataName;
      let fileReader = new FileReader();

      fileReader.readAsText(file);

      let promise = new Promise((resolve, error) => {
        fileReader.onload = (e) => {
          this.labeledDataManager.load(fileReader.result.toString());
          resolve(true);
        }
      }).then(() => {
        this.router.navigate(['model', this.labeledDataManager.modelType]);
        return true;
      },
        () => {
          return false;
        });

      let loadObservable = from(promise);

      this.progressSpinner.showProgressSpinnerUntilExecuted(loadObservable,
        res['filemenu.loading'], "assets/images/loading.gif",
        res['filemenu.loaded'], "");
    })

  }
}
