import { Component, OnInit } from '@angular/core';
import { LabeledDataManagerService } from 'src/app/services/labeled-data-manager.service';
import { ScratchManagerService } from 'src/app/services/scratch-manager.service';
import { MlAlgorithmService } from 'src/app/services/ml-algorithm.service';
import { FeatureExtractionService } from 'src/app/services/feature-extraction.service';
import { TestModelService } from 'src/app/services/test-model.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-ml-test-numerical-model',
  templateUrl: './ml-test-numerical-model.component.html',
  styleUrls: ['./ml-test-numerical-model.component.css']
})
export class MlTestNumericalModelComponent implements OnInit {

  testNumerical: string;
  prediction;
  mostLikelyClass: string;
  confidenceText: string;

  constructor(public labeledDataManager: LabeledDataManagerService,
    private featureExtractionService: FeatureExtractionService,
    private mlAlgorithmService: MlAlgorithmService,
    private scratchManager: ScratchManagerService,
    private testModelService: TestModelService,
    private snackBar: MatSnackBar,
    private translate: TranslateService) { }

  ngOnInit() {
  }

  test() {

    let keys_to_translate = [
      'model.atention',
      'model.numerical_input_wrong',
      'model.numerical_input_bad_dimension'
    ]

    this.translate.get(keys_to_translate).subscribe((res: string) => {

      let re = /^(\s*-?\d+(\.\d+)?)(\s*,\s*-?\d+(\.\d+)?)*$/;
      let inputIsOk = re.test(this.testNumerical);
      if (!inputIsOk) {
        this.snackBar.open(res['model.numerical_input_wrong'],
          res['model.atention'], {
          duration: 3000,
          verticalPosition: 'top'
        });
        return;
      }
      let featureDimension = this.testNumerical.split(",").length;
      if (featureDimension != this.labeledDataManager.featureDimension) {
        this.snackBar.open(res['model.numerical_input_bad_dimension'],
          res['model.atention'], {
          duration: 3000,
          verticalPosition: 'top'
        });
        return;
      }

      if (!this.testModelService.checkIfTestCanBeDone()) return;
      this.featureExtractionService.extractInstanceFeatures(this.testNumerical)
        .then(inputTensor => {
          this.mlAlgorithmService.classify(inputTensor).then(prediction => {
            this.prediction = prediction;
            this.testModelService.confidenceLevel(prediction[0][1], prediction[0][0])
              .subscribe((res: string) => {
                this.confidenceText = res;
              });
          });
        });

    });
  }

  loadScratch() {
    this.scratchManager.load();
  }

}
