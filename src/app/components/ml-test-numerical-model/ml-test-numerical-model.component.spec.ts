import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MlTestNumericalModelComponent } from './ml-test-numerical-model.component';

describe('MlTestNumericalModelComponent', () => {
  let component: MlTestNumericalModelComponent;
  let fixture: ComponentFixture<MlTestNumericalModelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MlTestNumericalModelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MlTestNumericalModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
