import { Component, OnInit, Inject, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Label, ExampleData } from 'src/app/interfaces/interfaces';
import { ScratchManagerService } from '../../services/scratch-manager.service';
import { LabeledDataManagerService } from '../../services/labeled-data-manager.service';
import { MlWebCamComponent } from '../ml-web-cam/ml-web-cam.component';
import { TranslateService } from '@ngx-translate/core';


export type DialogData = string;

const IMAGE_SIZE = 227;
@Component({
  selector: 'app-ml-label-container',
  templateUrl: './ml-label-container.component.html',
  styleUrls: ['./ml-label-container.component.css']
})
export class MlLabelContainerComponent implements OnInit {

  panelOpenState = true;
  @ViewChild('fileImagesElement', { static: true }) fileElement: ElementRef;
  @ViewChild('webcam') webcam: MlWebCamComponent;
  @Input('items') items: ExampleData[];
  @Input('label') label: Label;
  @Output() onChildDeleted = new EventEmitter<Label>();

  constructor(
    public labeledDataManager: LabeledDataManagerService,
    private scratchManager: ScratchManagerService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private translate: TranslateService) {

    this.items = [];

  }

  ngOnInit() {
  }

  addTerm() {

    let keys_to_be_translated = [
      'model.text_added'
    ];
    this.translate.get(keys_to_be_translated).subscribe((res: string) => {
      const dialogRef = this.dialog.open(MlLabelContainerDialogComponent, {
        width: '250px',
        data: ""
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result == "" || result == undefined) return;
        // This is the first text to be added in the label set
        this.items = this.items.concat([result]);
        let entry = { label: this.label, data: result };
        this.labeledDataManager.addEntry(entry);
        this.scratchManager.modelUpdated = false;
        this.snackBar.open(res["model.text_added"],
          '', {
          duration: 2000,
        });
      });

    });
  }

  addNumericalInput() {

    let keys_to_be_translated = [
      'model.numerical_input_added',
      'model.numerical_input_wrong',
      'model.numerical_input_bad_dimension'
    ];
    this.translate.get(keys_to_be_translated).subscribe((res: string) => {
      const dialogRef = this.dialog.open(MlLabelContainerDialogComponent, {
        width: '250px',
        data: ""
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result == "" || result == undefined) return;
        // This is the first text to be added in the label set
        let re = /^(\s*-?\d+(\.\d+)?)(\s*,\s*-?\d+(\.\d+)?)*$/;
        let inputIsOk = re.test(result);
        let message = inputIsOk ? "model.numerical_input_added" : "model.numerical_input_wrong";

        if (inputIsOk) {
          let featureDimension = result.split(",").length
          if (featureDimension == this.labeledDataManager.featureDimension) {
            this.items = this.items.concat([result]);
            let entry = { label: this.label, data: result };
            this.labeledDataManager.addEntry(entry);
            this.scratchManager.modelUpdated = false;
          } else {
            message = "model.numerical_input_bad_dimension";
          }
        }

        this.snackBar.open(res[message],
          '', {
          duration: 2000,
        });
      });

    });
  }


  addImages() {
    this.fileElement.nativeElement.click();
  }

  delete(text: string) {


    let keys_to_be_translated = [
      'model.text_deleted'
    ];
    this.translate.get(keys_to_be_translated).subscribe((res: string) => {
      const dialogRef = this.dialog.open(MlDeleteConfirmComponent, {
        width: '250px',
        data: text
      });

      dialogRef.afterClosed().subscribe(result => {
        //  Borrado inmutable de elemento
        if (result == undefined) return;
        this.items = this.items.filter(o => o != result);
        let entry = { label: this.label, data: result };
        this.labeledDataManager.removeEntry(entry);
        this.scratchManager.modelUpdated = false;
        this.snackBar.open(res['model.text_deleted'],
          '', {
          duration: 2000,
        });
      });
    })

  }

  deleteLabel() {
    let keys_to_be_translated = [
      'model.label_deleted'
    ];
    this.translate.get(keys_to_be_translated).subscribe((res: string) => {
      const dialogRef = this.dialog.open(MlDeleteConfirmComponent, {
        width: '250px',
        data: this.label
      });

      dialogRef.afterClosed().subscribe(result => {
        this.labeledDataManager.removeLabel(result);
        this.onChildDeleted.emit(result);

        this.snackBar.open(res['model.label_deleted'] + ' `' + result + '`',
          this.label, {
          duration: 2000,
        });
      });
    })

  }

  initWebcam() {
    this.webcam.enableVideo();
  }

  takeSnapshot(b64Img) {
    let image = new Image();
    image.src = b64Img;
    image.onload = () => {
      this.items = this.items.concat([image]);
      this.labeledDataManager.addEntry({ label: this.label, data: image });
    }
  }



  onLoaded(e) {
    let files = e.target.files;

    for (let file of files) {
      let fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onload = e => {
        let imageUrl = fileReader.result.toString();
        let image = new Image();
        image.src = imageUrl;
        image.onload = () => {
          this.items = this.items.concat(image);
          this.labeledDataManager.addEntry({ label: this.label, data: image });
        }

      }
    }
  }
}

@Component({
  templateUrl: 'ml-label-container-dialog.html',
})
export class MlLabelContainerDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<MlLabelContainerDialogComponent>,
    public labeledDataManager: LabeledDataManagerService,
    public translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  templateUrl: 'ml-confirm-dialog.html',
})
export class MlDeleteConfirmComponent {

  constructor(
    public dialogRef: MatDialogRef<MlDeleteConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}



