import { Component, OnInit } from '@angular/core';
import { IProject } from 'src/app/interfaces/interfaces';
import { ProjectManagerService } from 'src/app/services/project-manager.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { slideInAnimation } from 'src/app/animations';
import { TranslateService } from '@ngx-translate/core';
import { ConfigService } from 'src/app/services/config.service';

@Component({
  selector: 'app-ml-shared-projects',
  templateUrl: './ml-shared-projects.component.html',
  styleUrls: ['./ml-shared-projects.component.css'],
  animations: [slideInAnimation]
})
export class MlSharedProjectsComponent implements OnInit {

  loaded = false;
  projects: IProject[] = []
  constructor(private projectManager: ProjectManagerService,
    private snackBar: MatSnackBar,
    private translate: TranslateService) {
    if (localStorage.getItem("language") == null) {
      this.translate.setDefaultLang("es");
    } else {
      this.translate.setDefaultLang(localStorage.getItem("language"))
    }
  }

  ngOnInit() {
    this.projectManager.getSharedProjects().subscribe(
      v => {
        this.projects = v;
        this.loaded = true;
      },
      e => {
        this.snackBar.open("tienes que iniciar sesión antes para poder ver los proyectos compartidos",
          'Inicia sesión antes', {
          duration: 3000,
          verticalPosition: 'top'
        });
      }
    );
  }

  loadProject(project) {
    window.location.href = ConfigService.settings.easyml.url +
     '/model/' + project.type + '?id=' + project.id;
  }

  reinventProject(project) {
    project.name = project.name + '-copia';
    this.projectManager.reinventProject(project).subscribe(
      v => {
        this.loadProject(v);
      },
      e => {
        this.snackBar.open("tienes que iniciar sesión antes para poder reinventar proyectos",
          'Inicia sesión antes', {
          duration: 3000,
          verticalPosition: 'top'
        });
      }
    );

  }

}
