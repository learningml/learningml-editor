import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { LabeledDataManagerService } from 'src/app/services/labeled-data-manager.service';

const IMAGE_SIZE = 200;

@Component({
  selector: 'app-ml-web-cam',
  templateUrl: './ml-web-cam.component.html',
  styleUrls: ['./ml-web-cam.component.css']
})
export class MlWebCamComponent implements OnInit {

  cameraOn = false;
  toggleCameraText = 'model.turn-on.camera';
  card_class = 'cam-card';
  videoplayer = null;
  localstream = null;
  @ViewChild('videoContainer', { static: true }) videocontainer: ElementRef;

  @Input('title') title = "Webcam";
  @Input('size') size = IMAGE_SIZE;
  @Output('onCloseWebcam') onCloseWebcam = new EventEmitter<any>();
  @Output('onTakeSnapshot') onTakeSnapshot = new EventEmitter<string>();

  //image_size = IMAGE_SIZE;

  constructor(public labeledDataManager: LabeledDataManagerService, ) {
  }

  ngOnInit() {
    //this.enableVideo();
  }

  setCardClass(c){
    this.card_class = c;
  }

  enableVideo() {
    this.videoplayer = document.createElement('video');
    this.videocontainer.nativeElement.appendChild(this.videoplayer);
    navigator.mediaDevices.getUserMedia({ video: true, audio: false })
      .then((stream) => {
        this.localstream = stream;
        this.videoplayer.autoplay = true;
        this.videoplayer.srcObject = stream;
        this.videoplayer.width = IMAGE_SIZE;
        this.videoplayer.height = IMAGE_SIZE;
        this.cameraOn = true;
      });
  }

  disableVideo() {
    this.videoplayer.pause();
    this.videoplayer.src = "";

    while (this.videocontainer.nativeElement.firstChild) {
      this.videocontainer.nativeElement.removeChild(this.videocontainer.nativeElement.firstChild);
    }

    this.localstream.getTracks()[0].stop();
    this.videoplayer = null;
    this.localstream = null;
    this.cameraOn = false;

    this.onCloseWebcam.emit('close');
  }

  takeSnapshot() {
    let draw = document.createElement("canvas");
    draw.width = this.videoplayer.videoWidth;
    draw.height = this.videoplayer.videoHeight;
    let context2D = draw.getContext("2d");
    context2D.drawImage(this.videoplayer, 0, 0, this.videoplayer.videoWidth, this.videoplayer.videoHeight);
    
    let b64Img = draw.toDataURL('image/jpeg', 0.5);
    this.onTakeSnapshot.emit(b64Img);
    return b64Img;
  }

  toggleCamera() {

    if (!this.cameraOn) {
      this.enableVideo();
      this.toggleCameraText = 'model.turn-off.camera';
    } else {

      this.disableVideo();
      this.toggleCameraText = 'model.turn-on.camera';
    }
    this.cameraOn = !this.cameraOn;
  }

}
